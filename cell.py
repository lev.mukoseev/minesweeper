import enum


class Cell(enum.Enum):
    close_mine = 1
    open = 2
    flag = 3
    bombed = 4
    close_empty = 5
