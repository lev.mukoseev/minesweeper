import unittest
from field import Field


class FieldTest(unittest.TestCase):

    def test_init(self):
        with self.assertRaises(Exception):
            Field(2, 2, 5)

    def test_generate(self):
        field = Field(5, 5, 10)
        mines_count = len(field.mines)
        self.assertEqual(10, mines_count)
        self.assertEqual(10, field.mines_count)
