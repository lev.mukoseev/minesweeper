from field import Field


class Game:
    def __init__(self, field_h, field_w, bombs_count):
        self.field = Field(field_h, field_w, bombs_count)
        self.is_finished = False

    def make_turn(self, x, y, action):
        if action in ("open", "открыть"):
            if (x, y) in self.field.mines:
                print('Game over!')
                self.is_finished = True
            else:
                self.open_cell(x, y)
        elif action == "flag":
            self.field.field[y][x] = self.field.flag_skin
        else:
            print("Wrong action, try again!")

    def open_cell(self, x, y):
        n = self.field.get_neighbours(x, y)
        self.field.field[y][x] = str(n) + " "
        if n == 0:
            for dx in range(-1, 2):
                for dy in range(-1, 2):
                    if dx == dy == 0:
                        continue
                    if (0 <= x + dx < self.field.weight
                            and 0 <= y + dy < self.field.height
                            and self.field.field[y + dy][x + dx] == self.field.block_skin):
                        self.open_cell(x + dx, y + dy)

    def play(self):
        print(self.field)
        while True:
            x, y, action = input().split()
            self.make_turn(int(x) - 1, int(y) - 1, action)
            print(self.field)
            if self.field.is_finished() or self.is_finished:
                break
