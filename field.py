from random import randint


class Field:
    def __init__(self, weight: int, height: int, mines_count: int, block_skin='[]', mine_skin='**', flag_skin='|>'):
        if mines_count > weight * height:
            raise Exception("Слишком много мин")
        self.mine_skin = mine_skin
        self.block_skin = block_skin
        self.mines_count = mines_count
        self.height = height
        self.weight = weight
        self.mines = set()
        self.field = self.generate()
        self.flag_skin = flag_skin

    def generate(self):
        field = [[self.block_skin] * self.weight for _ in range(self.height)]
        while len(self.mines) < self.mines_count:
            x = randint(0, self.weight - 1)
            y = randint(0, self.height - 1)
            if (x, y) not in self.mines:
                # field[y][x] = self.mine_skin
                self.mines.add((x, y))
        return field

    def get_neighbours(self, x, y):
        count = 0
        for dx in range(-1, 2):
            for dy in range(-1, 2):
                if dx == dy == 0:
                    continue
                if (x + dx, y + dy) in self.mines:
                    count += 1
        return count

    def check_mines(self, x, y):
        pass

    def is_finished(self):
        pass

    def __str__(self):
        return "\n".join([" ".join(line) for line in self.field][::-1])
